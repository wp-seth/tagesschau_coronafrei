// ==UserScript==
// @name     tagesschau_coronafrei
// @description tagesschau ohne corona-news
// @include     https://www.tagesschau.de/*
// @version  1.1.1
// @grant    none
// ==/UserScript==

function replace_text(obj){
	obj.innerHTML = "blablablá"
}

function remove_corona(){
	const corona_re = /Corona|Covid[- ]?19|Lockdown|Pandemie|Impfstoff|Querdenke/;
	const headlines = Array();
	const sections = document.getElementsByClassName('teasergroup');
	for(let s = 0; s < sections.length; ++s){
		const teasers = sections[s].getElementsByClassName('teaser');
		for(let t = 0; t < teasers.length; ++t){
			const title_names = Array(
				'dachzeile', 'headline', 'teaser__headline', 'teaser__headline-wrapper', 
				'teaser__shorttext', 'teaser__topline', 'teasertext');
			for(let ti = 0; ti < title_names.length; ++ti){
				const titles = teasers[t].getElementsByClassName(title_names[ti]);
				if(titles.length > 0 && corona_re.test(titles[0].innerText)){
					headlines.push(teasers[t].getElementsByClassName("headline")[0]?.innerText);
					replace_text(teasers[t]);
					break;
				}
			}
		}
	}

	let lists = document.getElementsByClassName('list');
	for(let l = 0; l < lists.length; ++l){
		const children = lists[l].children;
		Array.from(children).forEach(li => {
				const cleaned_title = li.innerText.replace(/^[0-9]+\s+/, '').replace(/^[A-Za-z]+:\s+/, '');
				if(corona_re.test(li.innerText) || /coronavirus-/.test(li.innerHTML) 
						|| headlines.includes(li.innerText) 
						|| headlines.includes(cleaned_title)){
					replace_text(li);
				}
		});
	}
}

remove_corona();
