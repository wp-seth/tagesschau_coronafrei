# tagesschau_coronafrei

view tagesschau.de without (annoying) corona news

this script tries to replace every news about corona on tagesschau.de with "blablablá".

## prerequisites
- install firefox
- install [greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)

## installation
- go to [tagesschau_coronafrei.js](tagesschau_coronafrei.js)
- click on the icon for "copy file contents" or just copy the file content
- in firefox: greasemonkey -> new user script -> paste
